@if ($paginator->hasPages())
    <div class="paging paging--page clearfix">
        <div class="paging__wrap clearfix">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                {{-- <div class="paging__item">
                    <a class="paging__link paging__link--prev" href="javascript:void(0)">Prev</a>
                </div> --}}
            @else
                <div class="paging__item">
                    <a class="paging__link paging__link--prev" href="{{ $paginator->previousPageUrl() }}">Prev</a>
                </div>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <div class="paging__item">
                        <a class="paging__link" href="javascript:void(0)">{{ $element }}</a>
                    </div>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <div class="paging__item">
                                <a class="paging__link paging__link--active" href="javascript:void(0)">{{ $page }}</a>
                            </div>
                        @else
                            <div class="paging__item">
                                <a class="paging__link" href="{{ $url }}">{{ $page }}</a>
                            </div>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <div class="paging__item">
                    <a class="paging__link paging__link--next" href="{{ $paginator->nextPageUrl() }}">Next</a>
                </div>
            @else
                {{-- <div class="paging__item">
                    <a class="paging__link paging__link--next" href="javascript:void(0)">Next</a>
                </div> --}}
            @endif
        </div>
    </div>
@endif