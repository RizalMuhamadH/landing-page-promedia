<section class="about__section bg--grey" id="ig">
    <div class="container">
        <div class="mb-2 text-center">
            <h2>OUR FEED</h2>
            <h4>Update terbaru dari akun Official Promedia Teknologi Indonesia</h4>
            <h4>Follow akun Instagram kami di <a href="https://www.instagram.com/promediateknologi/" target="_blank">@promediateknologi</a></h4>
        </div>
        <div class="col-offset-fluid clearfix mb-2">
            @foreach ($ig_feed as $item)
                <div class="col-bs12-4 mb-2">
                    <div class="card card__news">
                        <a href="{{ $item['permalink'] }}" target="_blank">
                            <span class="card__ig__img img-ratio ratio1-1">
                                <img src="{{ $item['type'] == 'video' ? $item['thumbnail_url'] : $item['url'] }}" alt="Instagram">
                            </span>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
