<section class="about__section bg--grey" id="team">
    <div class="container">
        <div class="mb-2 text-center">
            <h2>PROMEDIA’s CHAIRMAN</h2>
            <h4>Orang-orang hebat dibalik tumbuh dan perkembangnya Promedia Teknologi Indonesia</h4>
        </div>
        <div class="about__team col-offset-fluid clearfix mb-2">
            @foreach ($chairman as $item)
            <div class="col-bs12-4 mb-2" style="height: 800px">
                <div class="about__team__item">
                    <div class="text-center">
                        <span class="card__ig__img img-ratio ratio9-16">
                            <img src="{{ $item['avatar'] }}" alt="">
                        </span>
                        <div class="mt-1">
                            <h4 class="text-center mt-1">{{ $item['name'] }}</h4>
                            <h6 class="text-center mb-1">{{ $item['description'] }}</h6>
                            <div class="inline-block text-center mt-1">
                                <div class="social__item">
                                    <a href="{{ $item['social_media']['facebook'] }}" class="social__link social__link--facebook">
                                        <span class="icon icon-facebook"></span>
                                    </a>
                                </div>
                                <div class="social__item">
                                    <a href="{{ $item['social_media']['twitter'] }}" class="social__link social__link--twitter">
                                        <span class="icon icon-twitter"></span>
                                    </a>
                                </div>
                                <div class="social__item">
                                    <a href="{{ $item['social_media']['instagram'] }}" class="social__link social__link--instagram">
                                        <span class="icon icon-instagram"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>