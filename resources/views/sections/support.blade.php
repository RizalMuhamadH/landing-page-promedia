<section class="about__section bg--grey" id="layanan">
    <div class="container">
        <div class="mb-2 text-center">
            <h2>OUR SUPPORT</h2>
            <h4>Promedia mendukung anda mewujudkan mimpi dengan sumber daya manusia dan teknologi yang cukup memadai dan menjanjikan.</h4>
        </div>
        <div class="hl mt3 clearfix">
            <div class="hl__small js--small">
                <div class="hl__s-item">
                    <div class="hl__s-box">
                        <h2 class="hl__s-title">Dukungan Sistem dan Teknologi Paling Terbarukan</h2>
                    </div>
                </div>
                <div class="hl__s-item">
                    <div class="hl__s-box">
                        <h2 class="hl__s-title">Dukungan Iklan dan Programmatic</h2>
                    </div>
                </div>
                <div class="hl__s-item">
                    <div class="hl__s-box">
                        <h2 class="hl__s-title">Dukungan Pengembangan Bisnis</h2>
                    </div>
                </div>
                <div class="hl__s-item">
                    <div class="hl__s-box">
                        <h2 class="hl__s-title">Dukungan Pelatihan</h2>
                    </div>
                </div>
                <div class="hl__s-item">
                    <div class="hl__s-box">
                        <h2 class="hl__s-title">Dukungan Sindikasi Konten</h2>
                    </div>
                </div>
            </div>
            <div class="hl__big hl__big--home js--big">
                <div class="hl__b-item">
                    Promedia Teknologi Indonesia bekerjasama dengan AWS sebagai penyedia layanan server handal dengan teknologi terbarukan. Menjanjikan performa dengan daya pacu yang tinggi serta keamanan data yang terjaga secara utuh.
                    {{-- <img src="asset/images/section/layanan-img1.jpg" alt="Layanan Web Server"> --}}
                </div>
                <div class="hl__b-item">
                    Mendukung iklan direct dan programmatic bagi para mitra yang tergabung dalam ekosistem, menjadikan salah satu ekosistem yang dipercaya dalam bisnis media online.
                    {{-- <img src="asset/images/section/layanan-img1.jpg" alt="Layanan Marketing"> --}}
                </div>
                <div class="hl__b-item">
                    Mendukung perkembangan bisnis mitra mulai dari pengembangan sumber daya ide, hingga sumber daya manusia dan legalitas perusahaan media.
                    {{-- <img src="asset/images/section/layanan-img1.jpg" alt="Layanan Customer Experience Design">						 --}}
                </div>
                <div class="hl__b-item">
                    Didukung oleh sumber daya yang berpengalaman dalam bidang jurnalistik, teknologi terbarukan, hingga manajemen konten untuk membantu kemajuan serta perkembangan media online mitra Promedia Teknologi Indonesia lewat pelatihan rutin. 
                    {{-- <img src="asset/images/section/layanan-img1.jpg" alt="Layanan Interactive Design & Product Development"> --}}
                </div>
                <div class="hl__b-item">
                    Mendukung proses sindikasi konten yang akurat, terkini, hingga mengedepankan sisi jurnalisme yang relevan dengan kebutuhan pembaca diseluruh penjuru negeri. 
                    {{-- <img src="asset/images/section/layanan-img1.jpg" alt="Layanan Content Management"> --}}
                </div>
            </div>
        </div>
    </div>
</section>