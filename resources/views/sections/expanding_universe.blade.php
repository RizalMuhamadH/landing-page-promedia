<section class="about__section" id="expanding">
    <div class="container">
        <div class="mb-2 text-center">
            <h2>EXPANDING UNIVERSE</h2>
            <h4>Promedia Teknologi Indonesia memiliki motto “Expanding Universe, The Future of Digital Media Ecosystem” yang artinya kami tidak hanya berkembang, tapi juga menjadi pembeda serta menciptakan ekosistem baru bagi para pengusaha media digital.</h4>
            <h4>Dilengkapi dengan pengalaman dan sumber daya yang profesional, menjadikan Promedia Teknologi Indonesia pionir ekosistem media digital di Indonesia yang terintegrasi, saling terhubung, serta memiliki jangkauan terluas.</h4>
        </div>
        <div class="about__expanding col-offset-fluid clearfix mb-2">
            <a href="#" class="about__expanding__item ratio1-1">
                <span class="img-ratio">
                    <img src="https://source.unsplash.com/400x400/?people" alt="">
                </span>
            </a>
            <a href="#" class="about__expanding__item ratio1-1">
                <span class="img-ratio">
                    <img src="https://source.unsplash.com/600x200/?city" alt="">
                </span>
            </a>
            <a href="#" class="about__expanding__item ratio1-1">
                <span class="img-ratio">
                    <img src="https://source.unsplash.com/600x200/?car" alt="">
                </span>
            </a>
        </div>
    </div>
</section>