<section class="about__section bg--pattern" id="mitra">
    <div class="container">
        <div class="mb-2 text-center">
            <h2>OUR PARTNER</h2>
            <h4>Mitra yang sudah bergabung dalam ekosistem Promedia Teknologi Indonesia (update 28 Oktober 2021 )</h4>
        </div>
        <div class="col-offset-fluid clearfix mb-2">
            @foreach ($client as $item)
                @if ($item['active'] == 1)
                    <div class="col-bs12-3 mb-2 text-center">
                        <div class="card__client">
                            <div class="card__client__img">
                                <a href="{{ $item['uri'] }}" target="_blank"><img src="{{ $item['logo'] }}"
                                        alt="{{ $item['name'] }}"></a>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        @if (Request::segment(1) != 'clients')
            <div class="mb-2 text-center">
                <a href="{{ route('clients') }}" class="button--primary button--primary--big p2">Lihat Lebih Banyak</a>
            </div>
        @endif
    </div>
</section>
