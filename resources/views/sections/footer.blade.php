<footer class="footer">
    <div class="row footer__bottom container clearfix">
        <div class="col-offset-fluid clearfix">
            <div class="col-bs12-8">
                <div class="footer__copyright">
                    <p>&copy; 2020 - 2021 Promedia Teknologi Indonesia</p>
                </div>
            </div>
            <div class="col-bs12-4">
                <div class="social social--header clearfix">
                    <div class="social__item">
                        <a href="#" class="social__link social__link--facebook">
                            <span class="icon icon-facebook"></span>
                        </a>
                    </div>
                    <div class="social__item">
                        <a href="#" class="social__link social__link--twitter">
                            <span class="icon icon-twitter"></span>
                        </a>
                    </div>
                    <div class="social__item">
                        <a href="#" class="social__link social__link--instagram">
                            <span class="icon icon-instagram"></span>
                        </a>
                    </div>
                    <div class="social__item">
                        <a href="#" class="social__link social__link--rss">
                            <span class="icon icon-rss"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>