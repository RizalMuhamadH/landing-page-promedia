<!-- content -->
<div class="intro__background">
    <img src="./asset/images/unsplash.jpg" alt="" style="height:100vh;width: 100%;" class="parallaxIntro">
    <div class="bg-shadow"></div>
</div>
<section class="intro">
    <div class="intro__title">
        <div class="row clearfix">
            <div class="container clearfix">
                <div class="intro__title__heading clearfix">
                    <h1>Let's make something extraordinary together</h1>
                    <h2>We have been collaborating with brands and agencies to build meaningful digital interactions
                        since 2007</h2>
                </div>
                <div class="intro__title__scroll">
                    Scroll Down<span class="icon icon-angle-down"></span>
                </div>
            </div>
        </div>
    </div>
</section>