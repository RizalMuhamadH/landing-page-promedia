<section class="about__section" id="statistik">
    <div class="container">
        <div class="mb-2 text-center">
            <h2>STATISTIK PROMEDIA TEKNOLOGI INDONESIA</h2>
            <h4>Perkembangan Ekosistem Promedia Teknologi Indonesia dalam angka (update xxxxxx)</h4>
        </div>
        <div class="row clearfix mb-2">
            @foreach ($statistic as $item)
            <div class="col-bs12-4 mb-2 text-center">
                <div class="count">
                    <div class="count__inside">
                        {{ $item['total'] }}
                    </div>
                </div>
                <h4>{{ $item['name'] }}</h4>
            </div>
            @endforeach
        </div>
    </div>
</section>