<section class="about__section" id="kontak">
    <div class="container">
        <div class="mb-2 text-center">
            <h2>CONTACT US</h2>
            <h4>Ingin terhubung lebih dekat dengan Promedia Teknologi Indonesia, silakan hubungi kami via formulir, nomor Whatsapp Official, atau lewat saluran telepon.</h4>
        </div>
        <div class="col-offset-fluid clearfix mb-2">
            <div class="col-bs12-6 mb-2">
                <div class="card card__form">
                    <h4 class="mb-3">Kontak Kami</h4>
                    <form action="{{ route('send.message') }}" method="POST" class="form mt2">
                        @csrf
                        <div class="form__group">
                            <input type="text" name="name" placeholder="Nama">
                        </div>
                        <div class="form__group">
                            <input type="text" name="email" placeholder="Email">
                        </div>
                        <div class="form__group">
                            <input type="text" name="subject" placeholder="Subjek">
                        </div>
                        <div class="form__group">
                            <textarea name="message" id="" cols="30" rows="10" placeholder="Isi"></textarea>
                        </div>
                        <div class="form__group">
                            <button class="form__button">KIRIM</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-bs12-6 mb-2">
                <h4 class="mb-3">Alamat Kantor Promedia Teknologi Indonesia</h4>
                <div class="mapouter mt2">
                    <div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31684.58965849197!2d107.60976506838645!3d-6.941441079025537!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e7fea1e69c83%3A0x1a1cbd847a0728c2!2sPromedia%20Teknologi%20Indonesia!5e0!3m2!1sen!2sid!4v1631848874545!5m2!1sen!2sid"
                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a
                            href="https://getasearch.com/nordvpn-coupon/"></a><br>
                        <style>
                            .mapouter {
                                position: relative;
                                text-align: right;
                                height: 300px;
                                width: 100%;
                                overflow: hidden;
                            }

                        </style><a href="https://www.embedgooglemap.net">make your own map google</a>
                        <style>
                            .gmap_canvas {
                                overflow: hidden;
                                background: none !important;
                                height: 500px;
                                width: 600px;
                            }

                        </style>
                    </div>
                </div>
                <div class="row mt2">
                    <div class="col-bs12-6">
                        <h4>Office Address</h4>
                        <p>Jalan Terusan Halimun No. 52, Lengkong,
                            Bandung, 40264
                            </p>
                    </div>
                    <div class="col-bs12-6">
                        <h4>Informasi Kontak</h4>
                        <p>
                            <b>Telp:</b> +62(22) 8735 7766 (Senin-Jumat 09:00 - 17:00 WIB) <br><br>
                            <b>Fax:</b> +62 811 2007 667 (Text Only Senin-Sabtu, 09:00 - 20:00 WIB) <br><br>
                            <b>Instagram:</b> <a href="https://instagram.com/promediateknologi" target="_blank" rel="noopener noreferrer">@promediateknologi</a> <br><br>
                            <b>Facebook:</b> <a href="https://facebook.com/promediateknologi" target="_blank" rel="noopener noreferrer">promediateknologi</a> <br><br>
                            <b>Email:</b> <a href="https://twitter.com/promediatekno" target="_blank" rel="noopener noreferrer">promediatekno</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
