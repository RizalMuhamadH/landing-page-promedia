<section class="about__section bg--grey" id="news">
    <div class="container">
        <div class="mb-2 text-center">
            <h2>LATEST NEWS FROM OUR PARTNER</h2>
            <h4>Kabar terbaru dan populer dari Mitra kami</h4>
        </div>
        <div class="col-offset-fluid clearfix mb-2">
            @foreach ($news['hits']['hits'] as $item)
                <div class="col-bs12-4 mb-2">
                    <div class="card card__news" style="height: 300px">
                        <a href="{{ $item['_source']['url'] }}" target="_blank">
                            <span class="card__news__img img-ratio ratio16-9">
                                <img src="{{ $item['_source']['thumb_url'] }}" alt="">
                            </span>
                            <span class="card__news__content">

                                <span class="card__news__content__wrap">
                                    <span
                                        class="card__news__content__date">{{ Carbon\Carbon::parse($item['_source']['published_date'])->format('d M') }}</span>
                                    <span>
                                        <h4 class="card__news__content__title">
                                            {{ \Illuminate\Support\Str::limit($item['_source']['title'], 70) }}</h4>
                                        <span class="card__news__content__author">Oleh:
                                            {{ $item['_source']['published_by']['name'] }} |
                                            {{ $item['_source']['site']['name'] }}</span>
                                    </span>
                                </span>
                                {{-- <p class="card__news__content__desc">{{ $item['_source']['description'] }}</p>
                            <a href="{{ $item['_source']['url'] }}" target="_blank" class="card__news__content__link">Continue Reading...</a> --}}
                            </span>
                        </a>
                    </div>
                </div>
            @endforeach

        </div>

        @if (Request::segment(1) != 'news')
            <div class="mb-2 text-center">
                <a href="{{ route('news') }}" class="button--primary button--primary--big p2">Lihat Lebih
                    Banyak</a>
            </div>
        @else
            {{ $pagination->links('pagination.default') }}
        @endif

    </div>
</section>
