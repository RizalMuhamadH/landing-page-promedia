<section class="about__section bg--primary-dark" id="testimoni">
    <div class="container">
        <div class="mb-2 text-center">
            <h2 class="text-white">Testimonial</h2>
            <h4 class="text-white">Testimoni dari beberapa client kami</h4>
        </div>
        <div class="col-offset-fluid clearfix mb-2">
            <div class="col-bs12-4 mb-2">
                <div class="about__testimoni">
                    <img src="https://source.unsplash.com/100x100/?girl" alt="" class="about__testimoni__ava">
                    <div class="about__testimoni__content text-center">
                        <p class="about__testimoni__p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        <h4 class="about__testimoni__author">Danny Aditya</h4>
                        <h6 class="about__testimoni__author--sub">Ayoindonesia.com</h6>
                    </div>
                </div>		
            </div>
            <div class="col-bs12-4 mb-2">
                <div class="about__testimoni">
                    <img src="https://source.unsplash.com/100x100/?man" alt="" class="about__testimoni__ava">
                    <div class="about__testimoni__content text-center">
                        <p class="about__testimoni__p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        <h4 class="about__testimoni__author">Danny Aditya</h4>
                        <h6 class="about__testimoni__author--sub">Ayoindonesia.com</h6>
                    </div>
                </div>		
            </div>
            <div class="col-bs12-4 mb-2">
                <div class="about__testimoni">
                    <img src="https://source.unsplash.com/100x100/?kid" alt="" class="about__testimoni__ava">
                    <div class="about__testimoni__content text-center">
                        <p class="about__testimoni__p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        <h4 class="about__testimoni__author">Danny Aditya</h4>
                        <h6 class="about__testimoni__author--sub">Ayoindonesia.com</h6>
                    </div>
                </div>		
            </div>
        </div>
    </div>
</section>