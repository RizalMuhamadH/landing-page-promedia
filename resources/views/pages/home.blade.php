@extends('layouts.master')

@section('head')
    <title>{{ $name }}</title>
@endsection

@section('content')
    @include('sections.header_home')
    @include('sections.statistic')
    @include('sections.support')
    {{-- @include('sections.testimonial') --}}
    @include('sections.expanding_universe')
    @include('sections.chairman')
    @include('sections.client')
    @include('sections.ig_feed')
    @include('sections.latest_news')
    @include('sections.contact')
    @include('sections.footer')
@endsection

@section('script')
<script src="./asset/js/slick.min.js"></script>
<script src="./asset/js/simpleParallax.min.js"></script>
<script>

    $(document).ready(function() {
        // slider
        $('.js--big').slick({
            autoplay: true,
            slidesToShow: 1,
            autoplaySpeed: 10000,
            pauseOnFocus: true,
            fade: true,
            dots: false,
            arrows: false,
            vertical: true,
            asNavFor: '.js--small',
            cssEase: 'ease',
        });
        $('.js--small').slick({
            asNavFor: '.js--big',
            slidesToShow: 5,
            adaptiveHeight: false,
            focusOnSelect: true,
            vertical: true,
            // responsive: [
            //     {
            //     breakpoint: 768,
            //     settings: {
                    
            //     }
            //     }
            // ]
        });
    });	

    var image = document.getElementsByClassName('parallaxIntro');
    var image2 = document.getElementsByClassName('parallaxAbout');
    var image3 = document.getElementsByClassName('parallaxAbout2');
    new simpleParallax(image, {
        
    });
    new simpleParallax(image2, {
        orientation: 'right',
        scale: 1.5,
    });
    new simpleParallax(image3, {
    });

    $('#js--menu').click(function(e){
        e.preventDefault();
        $('body').toggleClass('-menu');
        $(this).toggleClass('active');
        $('#js--nav').toggleClass('-hide');
    });
    $( "a.scrollLink" ).click(function( event ) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
        $('#js--nav').addClass('-hide');
        $('body').removeClass('-menu');
        $('#js--menu').removeClass('active');
    });
    
</script>
@endsection
