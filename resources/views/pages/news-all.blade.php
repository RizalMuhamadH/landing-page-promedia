@extends('layouts.master')

@section('head')
    <title>{{ $name }}</title>
@endsection

@section('content')
    @include('sections.latest_news')
    @include('sections.footer')
@endsection