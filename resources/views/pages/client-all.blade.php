@extends('layouts.master')

@section('head')
    <title>{{ $name }}</title>
@endsection

@section('content')
    @include('sections.client')
    @include('sections.footer')
@endsection