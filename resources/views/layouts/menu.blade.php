<header class="header header__about">
    <div class="header__middle">
        <div class="container row">
            <div class="col-offset-fluid clearfix">
                <div class="col-bs12-3">
                    <div class="logo">
                        <a href="/">
                            <img src="./asset/images/logo/logo-promedia.png" alt="Promedia Teknologi Indonesia">
                        </a>
                    </div>
                </div>
                <div class="col-bs12-9">
                    <div class="menu__item">
                        <a href="#sidr" class="menu__link icon-bar" id="js--menu">
                            <i></i>
                        </a>
                    </div>
                    <nav class="nav -hide" id="js--nav">
                        <ul>
                            {{-- <li><a href="{{ Request::segment(1) != null ? '/' : '' }}#statistik" class="scrollLink">Statistik</a></li> --}}
                            <li><a href="{{ Request::segment(1) != null ? '/' : '' }}#layanan" class="scrollLink">Our Support</a></li>
                            {{-- <li><a href="{{ Request::segment(1) != null ? '/' : '' }}#testimoni" class="scrollLink">Testimonial</a></li> --}}
                            <li><a href="{{ Request::segment(1) != null ? '/' : '' }}#mitra" class="scrollLink">Our Partner</a></li>
                            <li><a href="{{ Request::segment(1) != null ? '/' : '' }}#ig" class="scrollLink">Our Feed</a></li>
                            {{-- <li><a href="{{ Request::segment(1) != null ? '/' : '' }}#team" class="scrollLink">Chairman</a></li> --}}
                            {{-- <li><a href="#blog" class="scrollLink">Blog</a></li> --}}
                            <li><a href="{{ Request::segment(1) != null ? '/' : '' }}#news" class="scrollLink">Latest News From Our Partner</a></li>
                            <li><a href="{{ Request::segment(1) != null ? '/' : '' }}#kontak" class="scrollLink button--primary">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>