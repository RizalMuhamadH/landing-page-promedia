<?php

namespace App\Console;

use Dymantic\InstagramFeed\Profile;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(){
            try {
                Profile::where('username', 'promediateknologi')->first()->refreshFeed(6);
            } catch (Exception $e) {
                Log::error("Failed retrieving Instagram feed", ['message' => $e->getMessage()]);
            }
        })->twiceDaily(8, 16);

        $schedule->command("instagram-feed:refresh-token")->monthlyOn(15, '01:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
