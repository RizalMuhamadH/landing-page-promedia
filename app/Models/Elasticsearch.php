<?php

namespace App\Models;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Elasticsearch extends Model
{
    use HasFactory;

    public static function get($json){
        $data = json_encode($json);
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        return $client->request('GET', env('ES_HOST', '').'article/data/_search', ['body' => $data]);
    }
}
