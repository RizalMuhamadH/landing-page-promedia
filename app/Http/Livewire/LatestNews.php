<?php

namespace App\Http\Livewire;

use App\Traits\PaginationTrait;
use Livewire\Component;

class LatestNews extends Component
{
    use PaginationTrait;

    public $news = [];

    public $lastNews;

    public $page = 1;

    public function load($page)
    {
        $this->page = $page;

        $this->news = $this->paginate($this->lastNews['hits']['hits'], $this->page);
    }
    public function render()
    {
        return view('livewire.latest-news');
    }
}
