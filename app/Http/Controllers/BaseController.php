<?php

namespace App\Http\Controllers;

use App\Models\Elasticsearch;
use App\Traits\PaginationTrait;
use Dymantic\InstagramFeed\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class BaseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function home()
    {
        $name = "Landing Page";
        $path = base_path('database/seeders/');
        $client = json_decode(file_get_contents($path . "client.json"), true);
        $statistic = json_decode(file_get_contents($path . "statistic.json"), true);
        $chairman = json_decode(file_get_contents($path . "chairman.json"), true);

        $ig_feed = Profile::where('username', 'promediateknologi')->first()->feed(6);

        $news = Cache::remember('last_news', 600, function () {
            $data = Elasticsearch::get([
                "from"          => 0,
                "size"          => 6,
                "_source"       => ["title", "site.id", "site.name", "site.url", "section.id", "section.name","section.alias", "thumb_url", "photo_url", "published_by.name", "published_date", "url", "description"],
                "sort"          => [
                    [
                        "id"    => [
                            "order"   => "desc"
                        ]
                    ]
                ]
            ]);
            return $data->getBody()->getContents();
        });

        $news = json_decode($news, true);

        return view('pages.home', compact(['name', 'client', 'ig_feed', 'statistic', 'news', 'chairman']));
    }

    public function client_all()
    {
        
        $name = "Landing Page";
        $path = base_path('database/seeders/');
        $client = json_decode(file_get_contents($path . "client-all.json"), true);

        return view('pages.client-all', compact(['name', 'client']));
    }

    public function sendMessage(Request $request)
    {
        $response = Http::get('https://script.google.com/macros/s/AKfycbw4d_PiGU-XHc8WpyVkZ_Yck5fbawhnKceqzbXnKlEMhqI9cBoqyMBhvMaOHqRyO33Q/exec', [
            'name'          => $request->name,
            'email'         => $request->email,
            'subject'       => $request->subject,
            'message'       => $request->message
        ]);

        return redirect()->back()->withInput(['response' => $response]);
    }

    public function news(Request $request)
    {
        $page = $request->get('page') ? $request->get('page') : 1;

        $news = Cache::remember('news-'.$page, 60, function () use ($page) {
            $data = Elasticsearch::get([
                "from"          => ($page - 1) * 12,
                "size"          => 12,
                "_source"       => ["title", "site.id", "site.name", "site.url", "section.id", "section.name","section.alias", "thumb_url", "photo_url", "published_by.name", "published_date", "url", "description"],
                "sort"          => [
                    [
                        "id"    => [
                            "order"   => "desc"
                        ]
                    ]
                ]
            ]);
            return $data->getBody()->getContents();
        });

        $news = json_decode($news, true);

        return view('pages.news-all', ["pagination" => $this->paginate(10), "news" => $news, "name" => "Update berita terbaru dari mitra kami"]);
    }
}
