<?php

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BaseController::class, 'home']);
Route::get('/clients', [BaseController::class, 'client_all'])->name('clients');
Route::get('/news', [BaseController::class, 'news'])->name('news');
Route::middleware('throttle:message')->post('/send/message', [BaseController::class, 'sendMessage'])->name('send.message');
